//
//  AppDelegate.h
//  PGWS3DemoApp
//
//  Created by Yescas, Francisco on 11/5/15.
//  Copyright © 2015 Yescas, Francisco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

