//
//  main.m
//  PGWS3DemoApp
//
//  Created by Yescas, Francisco on 11/5/15.
//  Copyright © 2015 Yescas, Francisco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
