//
//  ViewController.m
//  PGWS3DemoApp
//
//  Created by Yescas, Francisco on 11/5/15.
//  Copyright © 2015 Yescas, Francisco. All rights reserved.
//

#import "ViewController.h"

#import <DTVASCore/DTVASCore.h>
#import <DTVASWSCore/DTVASWSCore.h>
#import <DTVPGWS3Core/DTVPGWS3Core.h>
#import <DTVEDM3Core/DTVEDM3Core.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [[DTVASWSStore sharedStore] appConfigurationOnHost:kDTVASWSHostProduction appID:@"navigator" appVersion:1 clientId:@"ipad" clientVersion:10 completion:^(BOOL configSuccess, NSString* configError)
     {
         [[DTVASStore sharedStore] loginWithUserName:@"morega41gg@swengtest.com" password:@"Dtvtest00" completion:^(BOOL loginSuccess, NSString* loginError) {
             [self testGroupService];
             [self testChannelService];
             [self testSeriesService];
             [self testContentService];
             [self testCelebrityService];
             [self testCacheStatusService];
             [self testUserAuthorizationService];
             [self testPPVOrderService];
             [self testRemoteBookingService];
             [self testReceiversService];
             [self testSplitZipCodeService];
             [self testServiceAttributesService];
             
             [self testEDMSearchService];
             [self testEDMTypeAheadSearchService];
             [self testEDMGetVersionService];
             [self testEDMGetCacheStatusService];
             [self testEDMRuleSearchService];
             [self testEDMGroupSearchService];
         }];
     }];
}

#pragma mark - 8.2 Group Service
-(void)testGroupService
{
    PGWS3ObjectMask* mask = [PGWS3ObjectMask maskWithObjectType:[PGWS3Group class]];
    [mask setRequestedFields:@[kGroupProgrammers, kGroupPackages] forObjectType:[PGWS3Group class]];
    PGWS3GroupParameters* groupParams = [PGWS3GroupParameters groupParametersWithChannelIDs:@[@"8249"] groupType:kPGWS3GroupTypeProgrammerGroup];
    [groupParams setMask:mask];
    [groupParams setScheduleStartTime:[NSDate date]];
    [groupParams setScheduleEndTime:[NSDate dateWithTimeIntervalSinceNow:60*60]];
//    groupParams.ott = kPGWS3OTTTypeOnly | kPGWS3OTTTypeInclude | kPGWS3OTTTypeIHC; // not working
//    groupParams.ottPreview = kPGWS3OTTPreviewTypeInclude; // not working
//    groupParams.include4k = kPGWS3Include4kTypeLinear; // not working
//    groupParams.device = kPGWS3DeviceTypePhone; // not working
//    groupParams.os = kPGWS3OSTypeIOS; // not working
    groupParams.checkHistory = true;
    groupParams.languageCode = kPGWS3LanguageTypeEnglish;
    groupParams.includePurchaseOffer = true;
    [[DTVPGWS3Store sharedStore] groups:groupParams response:^(NSArray* contents)
     {
         NSLog(@"Group Parameters %@", groupParams.parameters);
         NSLog(@"TEST groups: %@", contents);
     }];
}

#pragma mark - 8.3 Channel Service
-(void)testChannelService
{
    PGWS3ObjectMask* mask = [PGWS3ObjectMask maskWithObjectType:[PGWS3Channel class]];
    [mask setRequestedFields:@[kChannelId, kChannelShortName, kChannelCategory] forObjectType:[PGWS3Channel class]];
    PGWS3ChannelParameters* channelParams = [PGWS3ChannelParameters channelParametersWithScheduleStartTime:[NSDate date] scheduleEndTime:[NSDate dateWithTimeIntervalSinceNow:60*60]];
    //other initializers
    //PGWS3ChannelParameters* channelParams = [PGWS3ChannelParameters channelParametersWithChannelIDs:@[@"254", @"229", @"210"]];
    //PGWS3ChannelParameters* channelParams = [PGWS3ChannelParameters channelParametersWithChannelStartTime:[NSDate date] channelEndTime:[NSDate dateWithTimeIntervalSinceNow:60*60]];
    [channelParams setAuthorizedChannelsOnly:YES];
    channelParams.excludeDTVHomePage = YES;
    channelParams.livestreaming = kPGWS3LivestreamingTypeInHome;
    channelParams.formatOfFirstAiringChannel = kPGWS3FormatOfFirstAiringChannelTypeSDOnly;
    channelParams.mainCategory = @"News";
    channelParams.subCategory = @"CBS News Video";
    channelParams.numberOfChannels = 10;
    channelParams.orderBy = kPGWS3ChannelSortByChannelNameAscending;
    [[DTVPGWS3Store sharedStore] channels:channelParams response:^(NSArray* contents)
     {
         NSLog(@"Channel Parameters %@", channelParams.parameters);
         NSLog(@"TEST channels: %@", contents);
     }];
}

#pragma mark - 8.4 Series Service
-(void)testSeriesService
{
    PGWS3ObjectMask*  mask = [PGWS3ObjectMask maskWithObjectType:[PGWS3Series class]];
    [mask setRequestedFields:@[kSeriesId, kSeriesPremiere, kSeriesFinale, kSeriesOriginalNetwork] forObjectType:[PGWS3Series class]];
    PGWS3SeriesParameters *serieParameters = [PGWS3SeriesParameters seriesParametersWithSerieID:@"185450" seasons:@[@"12",@"13"]];
    //other initializers
    //PGWS3SeriesParameters *serieParameters = [PGWS3SeriesParameters seriesParametersWithSeriesIDs:@[@"185450", @"185595", @"10409716", @"8316200"]];
    serieParameters.mask = mask;
    [[DTVPGWS3Store sharedStore] series:serieParameters response:^(NSArray* contents)
     {
         NSLog(@"Series Parameters %@", serieParameters.parameters);
         NSLog(@"TEST series: %@", contents);
     }];
}

#pragma mark - 8.5 Content Service
-(void)testContentService
{
    PGWS3ContentParameters* contentParams = [PGWS3ContentParameters contentParametersWithTMSProgramIDs:@[@"SH007659180000", @"EP009357770098"]];
    //other initializers
    //PGWS3ContentParameters* contentParams = [PGWS3ContentParameters contentParametersWithChannelIDs:@[@"8249"] airTime:[NSDate date]];
    [[DTVPGWS3Store sharedStore] contents:contentParams response:^(NSArray* contents)
     {
         NSLog(@"Content Parameters %@", contentParams.parameters);
         NSLog(@"TEST contents: %@", contents);
     }];
}

#pragma mark - 8.6 Celebrity Service
-(void)testCelebrityService
{
    PGWS3ObjectMask* mask = [PGWS3ObjectMask maskWithObjectType:[PGWS3Celebrity class]];
    [mask setRequestedFields:@[kCelebrityFirstName, kCelebrityName, kCelebrityFilmography] forObjectType:[PGWS3Celebrity class]];
    PGWS3CelebrityParameters *celebrityParams = [PGWS3CelebrityParameters celebrityParametersWithPersonIDs:@[@"672017", @"854617", @"854623", @"846827"]];
    celebrityParams.includePurchaseOffer = true;
    celebrityParams.ott = kPGWS3OTTTypeICBS;
    celebrityParams.ottPreview = kPGWS3OTTPreviewTypeInclude;
    [[DTVPGWS3Store sharedStore] celebrities:celebrityParams response:^(NSArray* contents)
     {
         NSLog(@"Celebrity Parameters %@", celebrityParams.parameters);
         NSLog(@"TEST celebrities: %@", contents);
     }];
}

#pragma mark - 8.7 Cache Status Service
-(void)testCacheStatusService
{
    PGWS3ObjectMask* mask = [PGWS3ObjectMask maskWithObjectType:[PGWS3CacheStatus class]];
    [mask setRequestedFields:@[kCacheStatusEndTime, kCacheStatusEndTime, kCacheStatusStatus, kCacheStatusComment, kCacheStatusCacheSizeAfter] forObjectType:[PGWS3CacheStatus class]];
    PGWS3CacheStatusParameters *cacheParams = [PGWS3CacheStatusParameters cacheStatusParametersWithFilename:nil]; //missing test parameters
    cacheParams.mask = mask;
    [[DTVPGWS3Store sharedStore] cacheStatus:cacheParams response:^(NSArray * results) {
        PGWS3CacheStatus *cacheStatus = (PGWS3CacheStatus*)[results firstObject];
        NSLog(@"Cache Status Parameters %@", cacheParams.parameters);
        NSLog(@"TEST Cache Status: %@\n%@", cacheStatus, cacheStatus.cacheSizeAfter);
    }];
}

#pragma mark - 8.8 User Authorization Service
-(void)testUserAuthorizationService
{
    PGWS3ObjectMask* mask = [PGWS3ObjectMask maskWithObjectType:[PGWS3UserAuthorization class]];
    [mask setRequestedFields:@[kUserAuthorizationAuthorized, kUserAuthorizationMsg, kUserAuthorizationZipcode] forObjectType:[PGWS3UserAuthorization class]];
    PGWS3UserAuthorizationParameters *userAParams = [PGWS3UserAuthorizationParameters userAuthorizationParametersWithAccountID:@"" TMSStationID:@""]; //missing test parameters
    userAParams.mask = mask;
    userAParams.authOnly = true;
    [[DTVPGWS3Store sharedStore] userAuthorization:userAParams response:^(NSArray *results) {
        NSLog(@"User Authorization Parameters %@", userAParams.parameters);
        NSLog(@"TEST User Authorization: %@", results);
    }];
}

#pragma mark - 8.9 PPV Order Service
-(void)testPPVOrderService
{
    PGWS3ObjectMask* mask = [PGWS3ObjectMask maskWithObjectType:[PGWS3PPVOrder class]];
    [mask setRequestedFields:@[kPPVOrderCode, kPPVOrderMessage, kPPVOrderAdditionalMessage] forObjectType:[PGWS3PPVOrder class]];
    PGWS3PPVOrderParameters *ppvOParams = [PGWS3PPVOrderParameters PPVOrderParametersWithChannelID:@"8249" startTime:[NSDate date]];
    //other initializers
    //PGWS3PPVOrderParameters *ppvOParams = [PGWS3PPVOrderParameters PPVOrderParametersWithMaterialID:@"" deliveryType:kPPVOrderDeliveryTypePush];
    //PGWS3PPVOrderParameters *ppvOParams = [PGWS3PPVOrderParameters PPVOrderParametersWithTMSProgramID:@"" deliveryType:kPPVOrderDeliveryTypeVOPPPV];
    ppvOParams.mask = mask;
    [[DTVPGWS3Store sharedStore] PPVOrder:ppvOParams response:^(NSArray *results) {
        NSLog(@"PPV Order Parameters %@", ppvOParams.parameters);
        NSLog(@"TEST PPV Order: %@", results);
    }];
}

#pragma mark - 8.10 Remote Booking Service
-(void)testRemoteBookingService
{
    PGWS3ObjectMask* mask = [PGWS3ObjectMask maskWithObjectType:[PGWS3RemoteBooking class]];
    [mask setRequestedFields:@[kRemoteBookingStatus, kRemoteBookingStatusMessage, kRemoteBookingErrorCode, kRemoteBookingEToken, kRemoteBookingViewingAvailableAirTime] forObjectType:[PGWS3RemoteBooking class]];
    PGWS3RemoteBookingParameters *remoteBookingParams = [PGWS3RemoteBookingParameters remoteBookingParametersWithChannelID:@"" referenceID:@"" startTime:[NSDate date] accessCardID:@""]; //missing test parameters
    //other initializers
    //PGWS3RemoteBookingParameters *remoteBookingParams = [PGWS3RemoteBookingParameters remoteBookingParametersWithMaterialID:@"" accessCardID:@""];
    //PGWS3RemoteBookingParameters *remoteBookingParams = [PGWS3RemoteBookingParameters remoteBookingParametersWithTMSStationID:@"" accessCardID:@""];
    remoteBookingParams.mask = mask;
    [[DTVPGWS3Store sharedStore] remoteBooking:remoteBookingParams response:^(NSArray *results) {
        NSLog(@"Remote Booking Parameters %@", remoteBookingParams.parameters);
        NSLog(@"TEST Remote Booking: %@", results);
    }];
}

#pragma mark - 8.11 Receivers Service
-(void)testReceiversService
{
    PGWS3ObjectMask* mask = [PGWS3ObjectMask maskWithObjectType:[PGWS3HouseHold class]];
    [mask setRequestedFields:@[kHouseHoldBroadbandEnabled, kHouseHoldVodEnable, kHouseHoldReceiver] forObjectType:[PGWS3HouseHold class]];
    PGWS3ReceiversParameters *receiversParams = [PGWS3ReceiversParameters receiversParameters];
    receiversParams.mask = mask;
    [[DTVPGWS3Store sharedStore] receivers:receiversParams response:^(NSArray * results) {
        PGWS3HouseHold *houseHold = (PGWS3HouseHold*)[results firstObject];
        NSLog(@"Receivers Parameters %@", receiversParams.parameters);
        NSLog(@"TEST Receivers: %@\n%@", houseHold, houseHold.receivers);
    }];
}

#pragma mark - 8.12 Split Zip Code Service
-(void)testSplitZipCodeService
{
    PGWS3ObjectMask* mask = [PGWS3ObjectMask maskWithObjectType:[PGWS3SplitZipCode class]];
    [mask setRequestedFields:@[kSplitZipCode, kSplitZipCodeCountyName, kSplitZipCodeStateAbbrey, kSplitZipCodeMarketID] forObjectType:[PGWS3SplitZipCode class]];
    PGWS3SplitZipCodeParameters *zipCodeParams = [PGWS3SplitZipCodeParameters splitZipCodeParametersWithZipCode:@"90260"]; //missing test parameters
    zipCodeParams.mask = mask;
    [[DTVPGWS3Store sharedStore] splitZipCode:zipCodeParams response:^(NSArray * results) {
        NSLog(@"Split Zip Code Parameters %@", zipCodeParams.parameters);
        NSLog(@"TEST Split ZipCode: %@", results);
    }];
}

#pragma mark - 8.13	Service Attributes Service
-(void)testServiceAttributesService
{
    PGWS3ServiceAttributesParameters *serviceAttributesParams = [PGWS3ServiceAttributesParameters serviceAttributes];
    [[DTVPGWS3Store sharedStore] serviceAttributes:serviceAttributesParams response:^(NSArray * results) {
        NSLog(@"Service Attribute Parameters %@", serviceAttributesParams.parameters);
        NSLog(@"TEST Service Attribute: %@", results);
    }];
}

#pragma mark - 9.1 EDM Search Service
-(void)testEDMSearchService
{
    EDM3SearchParameters* parameters = [EDM3SearchParameters searchParametersWithStart:0 end:10];
    parameters.excludeAdultContent = YES;
    parameters.isCarousel = YES;
    parameters.format = kPGWS3FormatHD;
    parameters.channelIDs = @[@"199"];
    [[DTVEDM3Store sharedStore] search:parameters response:^(EDM3SearchResults * results) {
        NSLog(@"Search Parameters: %@", parameters.parameters);
        NSLog(@"TEST EDM search: %@", results);
    }];
}

#pragma mark - 9.3 Type Ahead Search Service
-(void)testEDMTypeAheadSearchService
{
    EDM3TypeAheadSearchParameters *parameters = [EDM3TypeAheadSearchParameters typeAheadSearchParametersWithSearchText:@"dad"];
    parameters.spellCheck = YES;
    [[DTVEDM3TypeAheadStore sharedStore] searchText:parameters response:^(EDM3TypeAheadSearchCollection * results) {
        NSLog(@"Type Ahead Search Parameters: %@", parameters.parameters);
        NSLog(@"TEST EDM type ahead search: %@", results.results);
    }];
}

#pragma mark - 9.4 Get Version Service
-(void)testEDMGetVersionService
{
    [[DTVEDM3TypeAheadStore sharedStore] getVersionWithResponse:^(NSString * version) {
        NSLog(@"TEST get version: %@", version);
    }];
}

#pragma mark - 9.5 Cache Status Service
-(void)testEDMGetCacheStatusService
{
    //implemented according to documentation (section 9.4) but not getting response
    [[DTVEDM3TypeAheadStore sharedStore] getCacheStatusWithResponse:^(NSString * cacheStatus) {
        NSLog(@"TEST get cache status: %@", cacheStatus);
    }];
}

#pragma mark - 9.6 EDM Rule Search Service
-(void)testEDMRuleSearchService
{
    PGWS3ObjectMask* mask = [PGWS3ObjectMask maskWithObjectType:[PGWS3Content class]];
    [mask setRequestedFields:@[kContentTmsID, kContentTitle, kContentChannels] forObjectType:[PGWS3Content class]];
    EDM3RuleSearchParameters * parameters = [EDM3RuleSearchParameters ruleSearchParametersWithRulename:@"AppHomeScreenTVShows" resultSetStart:0 resultSetEnd:10];
    parameters.mask = mask;
    [[DTVEDM3Store sharedStore] ruleSearch:parameters response:^(EDM3SearchResults* results)
     {
         NSLog(@"Rule Search Parameters: %@", parameters.parameters);
         NSLog(@"TEST EDM rule search: %@", results);
     }];
}

#pragma mark - 9.7 EDM Group Search Service
-(void)testEDMGroupSearchService
{
    PGWS3ObjectMask* mask = [PGWS3ObjectMask maskWithObjectType:[PGWS3Content class]];
    [mask setRequestedFields:@[kContentTmsID, kContentTitle, kContentChannels] forObjectType:[PGWS3Content class]];
    EDM3GroupSearchParameters *parameters = [EDM3GroupSearchParameters groupSearchParametersWithGroupname:@"TV_Shows_Android_All" resultSetStart:0 resultSetEnd:10];
    parameters.mask = mask;
    [[DTVEDM3Store sharedStore] groupSearch:parameters response:^(NSArray* results)
     {
         NSLog(@"Group Search Parameters: %@", parameters.parameters);
         NSLog(@"TEST EDM group search: %@", results);
     }];
}

@end
